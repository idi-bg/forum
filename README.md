## Logistics console application

The application is designed to serve to employees of a large Australian company aiming to expand its activities to the freight industry. The app can be used to manage the delivery of packages between hubs in major Australian cities. An employee of the company is able to record the details of a delivery package, create or search for suitable delivery routes, and inspect the current state of delivery packages, transport vehicles and delivery routes.

***

###### Developers: Georgi Dimitrov, Ivan Dimitrov and Stanislav Stanchev
    
***

#### Core functions

The application supports the following operations:

1. User can register, log in and log out.

2. Creating a delivery package – having unique id, start location, end location and weight in kg, and contact information for the customer.

3. Creating a delivery route – having unique id, and a list of locations (at least two).
    a. The first location is the starting location – it has a departure time.
    b. The other locations have expected arrival time.

4. Search for a route based on package’s start and end locations.

5. Updating a delivery route – assigns a free truck to it.

6. Updating a delivery route – assigns a delivery package.

7. View a information about routes, packages and trucks.

8. The program saves and a loads the data automatically. It also allows to reset the data manually.

### 1. User

In order to use the program, first you must be registered and logged in.
In order to register use the registeruser command followed by username, firstname, lastname, password userrole(Manager,Employee or Supervisor).
You can log out using the logout command.
You can log in using the login command, username and password.
The program always remembers the last logged in user.

### Example input

```
RegisterUser Ivan Ivan Petrov holaghj Manager
Login Ivan holaghj
Logout
```

### 2. Create a package


In order to create a package you must use the CreatePackage command followed by  start location, end location, weight, client_name, client_phone. Locations must be valid; Sydney, Melbourne, Adelaide, Alice Springs, Brisbane, Darwin, Perth. Start location and end 
location can't be the same.

### Example input

```
CreatePackage Melbourne Brisbane 5000 validname 1234567895
CreatePackage Melbourne Alice springs  1000 Ivan 1234567890
```
### 3. Creating delivery route

With the command "CreateRoute" a logged in employee could create delivery route covering the locations of the company in Australia: Sydney, Melbourne, Adelaide, Alice Springs, Brisbane, Darwin, Perth.

The app expects departure time from its starting location and a list of at least two delivery stops as input (example input is presented below).
The departure time should be in future. The correct departure time format is: DD-MMM-YYYY-HH and within range: 00 to 23.
No duplicate delivery stops allowed. No duplicate routes allowed - i.e. not allowed creation of route with exact same departure time and delivery stops. 
Once created, the app automatically calculates the distance and the estimated arrival time of the truck at each delivery stop. 

### Example input

```
CreateRoute 23-FEB-2024-18 Adelaide Melbourne Sydney Brisbane
CreateRoute 23-FEB-2024-18 Melbourne Sydney Brisbane
```

### 4. Search for a suitable route

With "SearchSuitableRoutes" command the app allows searching for a suitable route for package. I.e., the command could be used before package assignment to a specific route.
As an input, it requires two locations - a start and end location. 
Once executed, the app will show only suitable routes in progress (taking into account the starting location of the package).

### Example input

```
SearchSuitableRoutes Adelaide Sydney
SearchSuitableRoutes Sydney Brisbane
```

### 5. Assign a free truck to route.

In order to assign a free truck to existing route you need to write down a command "AssignTruckToRoad num"(num is the id of the route).
The program automatically get the most suitable truck and assign it to the current route.
We can also add a new truck if we want with give the command "AddTruck" followed by name, capacity, range.

### Example input

```
CreateRoute 23-FEB-2023-18 Alice Springs Adelaide Melbourne Sydney Brisbane
CreatePackage Alice Springs Melbourne 1000 Ivan 1234567890
AddTruck addtruck Man 25000 6000
AssignTruckToRoad 1
```

### 6. Assign a package to a delivery route.

In order to assign a package to a route you must use the AssignPackage command followed by the id of the package, 
the string 'to route' and the route id. The package and the route must be created. A truck must be assigned to the route.
Locations of package should be in the correct order(order of route stops) and must be in the route. You can't assign the 
same package to multiple routes.

### Example input

```
AssignPackage 2 to route 1
AssignPackage 3 to route 1
```

### 7. We have a few commands to view a information about routes, packages and trucks.

The command "PackageStatus" returns package info based on package id.
The command "UnassaignedPackages" shows all unassaigned packages
The command "ShowTruckStatus" shows which trucks are free and which are not, also the time when they will be available.
The command "showroutesinprogress" give us information about the route, each stop with its delivery weight. Also the next stop and the time.

### Example input

```
RegisterUser Ivan Ivan Petrov holaghj Manager
CreateRoute 23-FEB-2023-23 Alice Springs Adelaide Melbourne Sydney Brisbane
CreatePackage Alice springs Melbourne 1000 Ivan 1234567890
CreatePackage Melbourne Brisbane 5000 validname 1234567895
AssignTruckToRoad 1
AssignPackage 1 to route 1
PackageStatus 1
PackageStatus 1
UnassignedPackages
ShowRoutesInProgress
```

### 8. Automatic saving / manual resetting

The program saves and loads the data automatically. 

Also, with "ResetData" command an employee could manually reset the app data. In this case, the saved data would be lost.

### Example input

```
ResetData
```

### Application closing 

The program can be closed with "End" command.

```
End
```

### Disclaimer

Please note that running of the unittests will reset (null) the saved data and the information would be lost.
