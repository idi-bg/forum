from models.constants.user_roles import UserRoles

class User:

    USERNAME_LEN_MIN, USERNAME_LEN_MAX = 2, 20
    USERNAME_LEN_ERR = f'Username must be between {USERNAME_LEN_MIN} and {USERNAME_LEN_MAX} characters long!'
    USERNAME_INVALID_SYMBOLS = 'Username contains invalid symbols!'

    PASSWORD_LEN_MIN, PASSWORD_LEN_MAX = 5, 30
    PASSWORD_LEN_ERR = f'Password must be between {PASSWORD_LEN_MIN} and {PASSWORD_LEN_MAX} characters long!'
    PASSWORD_INVALID_SYMBOLS = 'Password contains invalid symbols!'

    LASTNAME_LEN_MIN, LASTNAME_LEN_MAX = 2, 20
    LASTNAME_LEN_ERR = f'Lastname must be between {LASTNAME_LEN_MIN} and {LASTNAME_LEN_MAX} characters long!'

    FIRSTNAME_LEN_MIN, FIRSTNAME_LEN_MAX = 2, 20
    FIRSTNAME_LEN_ERR = f'Firstname must be between {FIRSTNAME_LEN_MIN} and {FIRSTNAME_LEN_MAX} characters long!'

    def __init__(self, username: str, firstname: str, lastname: str, password: str, user_role: str):
        self._ensure_valid_username(username)
        self._ensure_valid_firstname(firstname)
        self._ensure_valid_lastname(lastname)
        self._ensure_valid_password(password)

        self._username = username
        self._firstname = firstname
        self._lastname = lastname
        self._password = password
        self._user_role = user_role

    @property
    def username(self):
        return self._username

    @property
    def firstname(self):
        return self._firstname

    @property
    def lastname(self):
        return self._lastname

    @property
    def password(self):
        return self._password

    @property
    def user_role(self):
        return self._user_role

    @property
    def is_manager(self):
        return self._user_role == UserRoles.MANAGER

    @property
    def is_supervisor(self):
        return self._user_role == UserRoles.SUPERVISOR

    def __str__(self):
        return f'Username: {self.username}, FullName: {self.firstname} {self.lastname}, Role: {self.user_role}'

    def _ensure_valid_password(self, password: str):
        if len(password) < User.PASSWORD_LEN_MIN or len(password) > User.PASSWORD_LEN_MAX:
            raise ValueError(User.PASSWORD_LEN_ERR)

        for ch in password:
            if not (ch.isalnum() or ch in '@*_-'):
                raise ValueError(User.PASSWORD_INVALID_SYMBOLS)
        
    def _ensure_valid_username(self, username: str):
        if len(username) < User.USERNAME_LEN_MIN or len(username) > User.USERNAME_LEN_MAX:
            raise ValueError(User.USERNAME_LEN_ERR)

        for ch in username:
            if not ch.isalnum():
                raise ValueError(User.USERNAME_INVALID_SYMBOLS)

    def _ensure_valid_firstname(self, firstname: str):
        if len(firstname) < User.FIRSTNAME_LEN_MIN or len(firstname) > User.FIRSTNAME_LEN_MAX:
            raise ValueError(User.FIRSTNAME_LEN_ERR)

    def _ensure_valid_lastname(self, lastname: str):
        if len(lastname) < User.LASTNAME_LEN_MIN or len(lastname) > User.LASTNAME_LEN_MAX:
            raise ValueError(User.LASTNAME_LEN_ERR)