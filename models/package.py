
# Creating a delivery package – unique id, start location, end location and weight in kg, and contact information for the customer.

from models.constants.package_status import Status
from errors.application_error import ApplicationError

class Package:

    def __init__(self, package_id: int, start_location: str, end_location:str, weight: int, client_name: str, client_phone: str):
        
        self._validate_weight(weight)
        self._validate_client_name(client_name)
        self._validate_client_phone(client_phone)

        self._id = package_id
        self._start_location = start_location
        self._end_location = end_location
        self._weight = weight
        self._client_name = client_name
        self._client_phone = client_phone
        self._status = Status.NOT_ASSIGNED
        self._route_number = None
        self._depart_time = 0
        self._arrival_time = 0

    @property
    def id(self):
        return self._id
    
    @property
    def start_location(self):
        return self._start_location
    @property
    def end_location(self):
        return self._end_location
         
    @property
    def weight(self):
        return self._weight
    @property
    def client_name(self):
        return self._client_name
    @property
    def client_phone(self):
        return self._client_phone
    @property
    def status(self):
        return self._status
    @property
    def depart_time(self):
        return self._depart_time
    
    @depart_time.setter
    def depart_time(self, value):
        self._depart_time = value 

    @property
    def arrival_time(self):
        return self._arrival_time

    @arrival_time.setter
    def arrival_time(self, value):
        self._arrival_time = value
        
    @property
    def route_number(self):
        return self._route_number
    
    @route_number.setter
    def route_number(self, value):
        self._route_number = value
    
    def change_status(self):
        self._status = Status.change_status(self.status)

    def _validate_weight(self, weight):
        if weight <= 0:
            raise ApplicationError('Weight cannot be zero or less!')
        elif weight > 42000:
            raise ApplicationError('Package is too heavy')
        
    def _validate_client_name(self, client_name):
        if len(client_name) <= 0:
            raise ApplicationError('Name cannot be empty!')
        
    def _validate_client_phone(self, client_phone):
        if len(client_phone) <= 9 and client_phone.isdigit():
            raise ApplicationError('Australian phone numbers have more than 9 digits!')
        
    def __str__(self):

        return f'''Package {self.id}, which belongs to {self.client_name}(phone number {self.client_phone}), has status [{self.status}].'''
        
    


    

