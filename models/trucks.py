from datetime import datetime
from models.constants.truck_status_params import TruckStatus
from errors.application_error import ApplicationError


class Trucks:
    _format = "%Y-%m-%d %H:%M"

    def __init__(self, truck_id: int, name: str, capacity: int, max_range: int):
        self._valid_capacity_and_range(capacity)
        self._valid_capacity_and_range(max_range)

        self._truck_id = truck_id
        self._name = name
        self._capacity = capacity
        self._max_range = max_range
        self._available_time = datetime.now()
        self._status = TruckStatus.FREE
        self._route_number = None

    @property
    def truck_id(self):
        return self._truck_id

    @property
    def truck_name(self):
        return self._name

    @property
    def capacity(self):
        return self._capacity

    @property
    def max_range(self):
        return self._max_range

    @property
    def available_time(self):
        return self._available_time

    @available_time.setter
    def available_time(self, value: datetime):
        if value < self._available_time.now():
            raise ValueError('Time cant be in the past.')
        self._available_time = value

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, value: TruckStatus):
        self._status = value

    @property
    def route_number(self):
        return self._route_number

    @route_number.setter
    def route_number(self, value):
        self._route_number = value

    def __str__(self):
        if self.status == TruckStatus.FREE:
            return f'Truck with id: {self.truck_id}, capacity: {self.capacity} and max_range: {self.max_range} is free.'
        elif self.status == TruckStatus.ON_ROAD:
            return f'Truck with id: {self.truck_id}, capacity: {self.capacity} and max_range: {self.max_range} is ' \
                   f'assign on route {self.route_number} and will be available' \
                   f' after {self.available_time.strftime(self._format)}.'

    def _valid_capacity_and_range(self, value):
        if value <= 0:
            raise ApplicationError(f"Value must be positive.")
