import datetime
from errors.application_error import ApplicationError
from models.constants.hubs_and_distance import HubsAndDistance

class Route:

    AVG_SPEED = 87

    def __init__(self, id: int, departure_time: datetime, route_stops: list[str]):

        self._concatenate_if_aliceSprings_exists(route_stops)
        self._validate_locations(route_stops)

        self._route_id: int = id
        self._departure_time: datetime = departure_time
        self._route_stops: list[str] = route_stops
        self._stops_and_time: dict = self.route_creation(self._departure_time, self._route_stops)

    @property
    def id(self) -> int:
        return self._route_id

    @property
    def departure_time(self) -> datetime:
        return self._departure_time

    @property
    def route_stops(self) -> list:
        return self._route_stops

    @property
    def stops_and_time(self) -> dict:
        return self._stops_and_time

    @property
    def total_time(self) -> int:
        return self.total_distance // Route.AVG_SPEED

    @property
    def total_distance(self) -> int:
        distance = 0
        for indx_city in range(len(self._route_stops)-1):
            distance += HubsAndDistance.get_distance(self._route_stops[indx_city], self._route_stops[indx_city+1])
        return distance

    def route_creation(self, departure_time: datetime, route_stops: list) -> dict:
        '''Returns estimated arrival rime on each route stop'''
        dt = departure_time
        result = dt.strftime("%Y-%m-%d %H:%M")
        stops_and_time = {route_stops[0]:result}

        for indx in range(len(route_stops) - 1):
            route_stop = route_stops[indx]
            distance = HubsAndDistance.get_distance(route_stop, self.route_stops[indx+1])
            expected_time = distance / Route.AVG_SPEED
            expected_arrival_time = dt + datetime.timedelta(hours=expected_time)
            stops_and_time[route_stops[indx+1]] = expected_arrival_time.strftime("%Y-%m-%d %H:%M")
            dt = expected_arrival_time

        return stops_and_time

    def to_string(self):
        return '\n'.join([f'{self}'] + [f'  City: {key} : Arrival time {value}' for key, value in self.stops_and_time.items()])

    def _validate_locations(self, route_stops: list[str]) -> list[str] | ApplicationError:
        '''Locations are valid if: the company has office in the listed city, no duplicates and if consecutive locations are different.
        Raises an ApplicationError if not valid.'''
        if len(route_stops) < 2:
            raise ApplicationError('Invalid number of locations. The route should have at least two locations')
        if any(city for city in route_stops if route_stops.count(city) > 1):
            raise ApplicationError('Invalid route stop locations. No duplicates allowed!')
        if all([stop.lower() in HubsAndDistance.HUBS.keys() for stop in route_stops]):
            if all([route_stops[indx] != route_stops[indx+1] for indx in range(len(route_stops)-1)]):
                return route_stops
        else:
            raise ApplicationError('Invalid route stop locations. Please enter valid stop locations.')

    def _concatenate_if_aliceSprings_exists(self, route_stops: list[str]) -> list[str]:
        '''Returns concatenated 'Alice Springs' if the two consecutive words are given as input.'''
        for i, city in enumerate(route_stops):
            if city.lower() == 'alice' and route_stops[i+1].lower() == 'springs':
                route_stops[i] = 'Alice Springs'
                route_stops.pop(i + 1)
        return route_stops

    def __str__(self):
        output = []
        for key, value in self.stops_and_time.items():
            output.append(f'{key} ({value})')
        output.insert(0, f'Route # {self.id}')
        return ' -> '.join(output)

    def __len__(self):
        return len(self._route_stops) 