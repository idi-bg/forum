class UserRoles:

    MANAGER = 'Manager'
    EMPLOYEE = 'Employee'
    SUPERVISOR = 'Supervisor'
    _roles = [MANAGER, EMPLOYEE, SUPERVISOR]
    @classmethod
    def from_string(cls, value) -> str:
        if value  in cls._roles:
           return value
        raise ValueError(
                f'None of the possible UserRole values matches the value {value}.')