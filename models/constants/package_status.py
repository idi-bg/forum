class Status():
    NOT_ASSIGNED = 'Not assigned'
    ASSIGNED = 'Assigned'
    ARRIVED = 'Arrived'

    _status = (NOT_ASSIGNED, ASSIGNED, ARRIVED)

    @classmethod
    def change_status(cls, current):
        idx = cls._status.index(current)
        return cls._status[idx + 1]
        