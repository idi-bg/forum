class TruckStatus:
    FREE = 'Free'
    ON_ROAD = 'On road'

    @classmethod
    def from_string(cls, value) -> str:
        if value not in [cls.FREE, cls.ON_ROAD]:
            raise ValueError(
                f'Not such status as {value}')

        return value

