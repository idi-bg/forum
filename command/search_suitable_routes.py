from command.base.base_command import BaseCommand
from core.application_data import ApplicationData 
from core.userapplication import UserApplication

class SearchSuitableRoutes(BaseCommand):
    
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def execute(self, params: list[str]):
        super().execute(params)

        self._concatenate_if_aliceSprings_exists(params)
        self._try_validate_stops(params)
        
        start_location, end_location = params[0], params[1]

        suitable_routes = self._app_data.get_suitable_routes(start_location, end_location)

        if len(suitable_routes) == 0:
            return f'No suitable routes with start location: {start_location} and end location: {end_location}'

        return '\n'.join(str(route) for route in suitable_routes)
        
    def _expected_params_count(self) -> int:
        return 2
    
    def _requires_login(self) -> bool:
        return True