from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.userapplication import UserApplication

class UnassaignedPackages(BaseCommand):
    
    def __init__(self, 
                 app_data: ApplicationData,
                 user_application:UserApplication):
        
        super().__init__(app_data, user_application)
        
    
    def _expected_params_count(self) -> int:
        return 0
    
    def _requires_login(self) -> bool:
        return True

    def execute(self, params): 
        super().execute(params)
        
        is_manager = self._user_application.logged_in_user.is_manager
        is_supervisor = self._user_application.logged_in_user.is_supervisor
        if not (is_manager or is_supervisor):

            raise ValueError('You are not a manager or a supervisor!')
        
        return self._app_data.show_unassaigned_packages()