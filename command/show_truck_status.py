from core.application_data import ApplicationData
from command.base.base_command import BaseCommand
from core.userapplication import UserApplication

class ShowTruckStatus(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application : UserApplication):
        super().__init__(app_data, user_application)

    def _expected_params_count(self) -> int:
        return 0

    def execute(self, params) -> str:
        super().execute(params)

        return self._app_data.view_information_about_trucks()
    
    def _requires_login(self) -> bool:
        return True