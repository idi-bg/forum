from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from models.constants.user_roles import UserRoles
from core.userapplication import UserApplication

class RegisterUser(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)


    def execute(self, params: list[str]):
        super().execute(params)
        self._throw_if_user_logged_in()

        username, firstname, lastname, password, userrole = params

        user_role = UserRoles.from_string(userrole)

        user = self._user_application.create_user(
            username, firstname, lastname, password, user_role)
        self._user_application.login(user)

        return f'User {user.username} ({user_role}) registered successfully!'

    def _requires_login(self) -> bool:
        return False

    def _expected_params_count(self) -> int:
        return 5