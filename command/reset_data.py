from command.base.base_command import BaseCommand
from core.processing_data import ProcessingData
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from core.userapplication import UserApplication
from command.starting_command import Starting

class ResetData(BaseCommand):

    def __init__(self, processing_data: ProcessingData, app_data: ApplicationData, user_application: UserApplication, model_factory: ModelsFactory):
        self._processing_data = processing_data
        self._models_factory = model_factory
        super().__init__(app_data, user_application)

    def _requires_login(self) -> bool:
        return True

    def _expected_params_count(self) -> int:
        return 0
        
    def execute(self, _):
    
        if self._user_application.logged_in_user:
            self._processing_data.reset_data()
            loading_trucks = Starting(self._app_data,self._user_application, self._models_factory)
            loading_trucks.execute('')
            return "The Logistic Application has been reset!"

