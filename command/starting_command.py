from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from core.userapplication import UserApplication

class Starting(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication, model_factory: ModelsFactory):
        super(Starting, self).__init__(app_data, user_application)
        self._model_factory = model_factory

    @property
    def model_factory(self):
        return self._model_factory

    def _expected_params_count(self) -> int:
        return 0

    def execute(self, params):

        if not self._app_data.trucks:
            for i in range(10):
                new_truck = self.model_factory.create_truck('Scania', 42000, 8000)
                self._app_data.add_truck(new_truck)

            for i in range(15):
                new_truck = self.model_factory.create_truck('Man', 37000, 10000)
                self._app_data.add_truck(new_truck)

            for i in range(15):
                new_truck = self.model_factory.create_truck('Actros', 26000, 13000)
                self._app_data.add_truck(new_truck)

            return f'System ready.'

        else:
            return f"Data loaded"
