from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from core.userapplication import UserApplication


class AddTruck(BaseCommand):
    def __init__(self, app_data: ApplicationData, models_factory: ModelsFactory, user_application: UserApplication):
        super().__init__(app_data, user_application)
        self._models_factory = models_factory

    def _expected_params_count(self) -> int:
        return 3

    def _requires_login(self) -> bool:
        return True

    def execute(self, params: list[str]):
        super().execute(params)

        name = params[0]
        capacity = self._try_parse_int(params[1])
        max_range = self._try_parse_int(params[2])
        if capacity <= 0 or max_range <= 0:
            raise ValueError("The value must be positive.")

        new_truck = self._models_factory.create_truck(name, capacity, max_range)
        self._app_data.add_truck(new_truck)
        return f'Truck {new_truck.truck_name} with id {new_truck.truck_id} is added.'
