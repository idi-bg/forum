

from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from core.userapplication import UserApplication

class CreatePackage(BaseCommand):
    
    def __init__(self, 
                 app_data: ApplicationData, user_application: UserApplication,
                 models_factory: ModelsFactory):
        super().__init__(app_data, user_application)
        self._models_factory = models_factory

    @property
    def models_factory(self):
        return self._models_factory
    
    def _expected_params_count(self) -> int:
        return 5
    def _requires_login(self) -> bool:
        return True

    def execute(self, params):
        super().execute(params)
        #validate locations
        #client phone str?
        self._concatenate_if_aliceSprings_exists(params)
        start_location = params[0]
        end_location = params[1]
        self._try_validate_stops([start_location, end_location])
        weight = self._try_parse_int(params[2])
        client_name = params[3]
        client_phone = str(params[4])
        package = self.models_factory.create_package(start_location, end_location, weight, client_name, client_phone)
        self._app_data.add_package(package)

        return f'Package with id {package.id} created.'
    

    