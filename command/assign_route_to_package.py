from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from errors.application_error import ApplicationError
from core.userapplication import UserApplication
class AssignPackage(BaseCommand):
    
    def __init__(self, 
                 app_data: ApplicationData,
                 user_application: UserApplication ):
        super().__init__(app_data, user_application)
        

    def _expected_params_count(self) -> int:
        return 4
    
    
    def execute(self, params):
        super().execute(params)
        
        package_id = self._try_parse_int(params[0])
        route_id = self._try_parse_int(params[3])
        package = self._app_data.search_package_by_id(package_id)
        self._app_data.check_locations_package(package, route_id)
        check_for_truck = self._app_data.search_truck_by_route_id(route_id)
        check_for_route = package.status == 'Not assigned'
        if not check_for_route:
            raise ApplicationError(f'Package already assigned to route {package.route_number}')
        if check_for_truck != None and self._app_data.check_weight_to_add_package(route_id, package):
            self._app_data.assign_route_to_package(package, route_id)
            return f'Package {package.id} assigned to route {route_id}'
        else:
            raise ApplicationError('There is no assigned truck to this route!')

    def _requires_login(self) -> bool:
        return True


        