from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from errors.application_error import ApplicationError
from core.userapplication import UserApplication

class AssignTruckToRoad(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def _expected_params_count(self) -> int:
        return 1

    def execute(self, params) -> str:
        super().execute(params)
        route_id = self._try_parse_int(params[0])
        route = self._app_data.search_route_by_id(route_id)
        check = self._app_data.search_truck_by_route_id(route_id)
        if check == None:
            truck = self._app_data.assign_truck_to_route(route)
        else:
            raise ApplicationError('Only one truck can be assigned to route!')

        return f'Truck with id {truck.truck_id} is assign to road {route_id}'
    
    def _requires_login(self) -> bool:
        return True