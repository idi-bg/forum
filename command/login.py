from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.userapplication import UserApplication

class LogIn(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def execute(self, params: list[str]):
        super().execute(params)
        
        self._throw_if_user_logged_in()
        username, password = params
        user = self._user_application.find_user_by_username(username)
        if user.password != password:
            raise ValueError('Wrong username or password!')
        else:
            self._user_application.login(user)

            return f'User {user.username} successfully logged in!'

    def _requires_login(self) -> bool:
        return False

    def _expected_params_count(self) -> int:
        return 2