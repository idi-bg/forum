from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.userapplication import UserApplication

class ShowUsers(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def execute(self, params: list[str]):
        super().execute(params)

        if not self._user_application.logged_in_user.is_manager:
            raise ValueError('You are not a manager!')

        output = ['--USERS--']
        for i, user in enumerate(self._user_application.users):
            output.append(f'{i + 1}. {user}')

        return '\n'.join(output)

    def _requires_login(self) -> bool:
        return True

    def _expected_params_count(self) -> int:
        return 0