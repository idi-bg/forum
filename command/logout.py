from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.userapplication import UserApplication

class LogOut(BaseCommand):
    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def execute(self, params: list[str]):
        super().execute(params)
        
        self._user_application.logout()

        return 'You logged out!'

    def _requires_login(self) -> bool:
        return True

    def _expected_params_count(self) -> int:
        return 0