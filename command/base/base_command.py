import datetime
from core.application_data import ApplicationData
from models.constants.hubs_and_distance import HubsAndDistance
from errors.application_error import ApplicationError
from core.userapplication import UserApplication

class BaseCommand:

    def __init__(self, app_data: ApplicationData, user_application:UserApplication):
        self._app_data = app_data
        self._user_application = user_application

    def execute(self, params) -> str:
        if self._requires_login() and not self._user_application.has_logged_in_user:
            raise ApplicationError('You are not logged in! Please login first!')

        if len(params) < self._expected_params_count():
            raise ApplicationError(
                f'Invalid number of arguments. Expected at least {self._expected_params_count()}; received: {len(params)}.')

        return ''
    
    def _requires_login(self) -> bool:
        raise NotImplementedError('Override in derived class')
    
    def _throw_if_user_logged_in(self):
        if self._user_application.has_logged_in_user:
            logged_user = self._user_application.logged_in_user
            raise ApplicationError(
                f'User {logged_user.username}({logged_user.user_role}) is logged in! Please log out first!')
    
    def _try_parse_float(self, s, msg='Invalid value. Expected a float.'):
        try:
            return float(s)
        except:
            raise ValueError(msg)

    def _try_parse_int(self, s, msg='Invalid value. Expected an int.'):
        try:
            return int(s)
        except:
            raise ValueError(msg)

    def _expected_params_count(self) -> int:
        raise NotImplementedError('Override in derived class')

    def _try_parse_datetime(self, s):
        date_format = '%d-%b-%Y-%H'
        s = s.upper()
        try:
            parsed_date = datetime.datetime.strptime(s, date_format)
            return parsed_date
        except ValueError:
            raise ApplicationError('Invalid value for date and time. Should be in format: DD-MMM-YYYY-HH and within range: 00 to 23.')
        
    def _try_validate_stops(self, route_stops: list[str]) -> list[str] | ApplicationError:
        '''Checks if the company has office in the listed locations and if consecutive locations are different.'''
        if len(route_stops) < 2:
             raise ApplicationError('Invalid number of locations. The route should have at least two locations')
        if any(city for city in route_stops if route_stops.count(city) > 1):
            raise ApplicationError('Invalid route stop locations. No duplicates allowed!')
        if all([stop.lower() in HubsAndDistance.HUBS.keys() for stop in route_stops]):
            if all([route_stops[indx] != route_stops[indx+1] for indx in range(len(route_stops)-1)]):
                return route_stops
        else:
            raise ApplicationError('Invalid route stop locations. Please enter valid stop locations.')
        
    def _concatenate_if_aliceSprings_exists(self, route_stops):

        for i, city in enumerate(route_stops):
            if city.lower() == 'alice' and route_stops[i+1].lower() == 'springs':
                route_stops[i] = 'Alice Springs'
                route_stops.pop(i + 1)
        return route_stops