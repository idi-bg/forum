from command.base.base_command import BaseCommand
from core.application_data import ApplicationData 
from core.userapplication import UserApplication
from errors.application_error import ApplicationError

class ShowRoutesInProgress(BaseCommand):

    def __init__(self, app_data: ApplicationData, user_application: UserApplication):
        super().__init__(app_data, user_application)

    def execute(self, params):
        super().execute(params)
        if self._user_application.logged_in_user.is_manager:
           manager_view = self._app_data.manager_view()
           return manager_view
        else:
            raise ApplicationError('You dont have access to this information(only for managers)!')
        
    def _expected_params_count(self) -> int:
        return 0
    def _requires_login(self) -> bool:
        return True
   