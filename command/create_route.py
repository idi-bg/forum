import datetime
from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from errors.application_error import ApplicationError
from core.userapplication import UserApplication

class CreateRoute(BaseCommand):
    
    def __init__(self, 
                 app_data: ApplicationData, user_application: UserApplication,
                 models_factory: ModelsFactory):
        super().__init__(app_data, user_application)
        self._models_factory = models_factory

    @property
    def models_factory(self):
        return self._models_factory

    def execute(self, params: list[str]):
        super().execute(params)
        route = self._create_route(params)
        self._app_data.add_route(route)

        return f'Delivery route #{route.id} with departure time {route.departure_time} and {len(route)} locations was created!'

    def _expected_params_count(self) -> int:
        return 3
    
    def _requires_login(self) -> bool:
        return True

    def _create_route(self, params: list[str]):

        departure_time, *route_stops = params
        self._concatenate_if_aliceSprings_exists(route_stops)
        self._try_validate_stops(route_stops)

        if self._app_data.route_exists(departure_time, route_stops):
            raise ValueError(f'Route with such departure time and locations already exists!')

        departure_time = self._try_parse_datetime(departure_time) 
        self._proceed_if_departure_time_in_future(departure_time)

        route = self._models_factory.create_route(departure_time, route_stops) 
            
        return route

    def _proceed_if_departure_time_in_future(self, parsed_date) -> bool:
        now = datetime.datetime.now()
        now_str = now.strftime('%d-%b-%Y-%H')
        parsed_date_str = parsed_date.strftime('%d-%b-%Y-%H')
      
        if parsed_date_str > now_str:
            return True
        raise ApplicationError('Departure time should be in future.')
