from command.base.base_command import BaseCommand
from core.application_data import ApplicationData
from core.userapplication import UserApplication

class PackageStatus(BaseCommand):
    
    def __init__(self, 
                 app_data: ApplicationData,
                 user_application:UserApplication):
        
        super().__init__(app_data, user_application)
        
    @property
    def models_factory(self):
        return self._models_factory
    
    def _expected_params_count(self) -> int:
        return 1
    
    def _requires_login(self) -> bool:
        return True

    def execute(self, params):
        super().execute(params)
        package_id = self._try_parse_int(params[0])
        return self._app_data.show_package_info(package_id)



    

