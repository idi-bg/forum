import unittest

import command.register_user
from core.models_factory import ModelsFactory
from core.application_data import ApplicationData
from core.userapplication import UserApplication
from errors.application_error import ApplicationError
from core.command_factory import CommandFactory
from core.processing_data import ProcessingData
from core.userapplication import UserApplication
from command.add_truck import AddTruck
from command.starting_command import Starting
from command.create_package import CreatePackage
from command.search_suitable_routes import SearchSuitableRoutes
from command.assign_route_to_package import AssignPackage
from command.show_truck_status import ShowTruckStatus
from command.show_package_status import PackageStatus
from command.show_routes_in_progress import ShowRoutesInProgress
from command.register_user import RegisterUser
from command.login import LogIn
from command.logout import LogOut
from command.show_users import ShowUsers
from command.show_unassigned_packages import UnassaignedPackages
from command.reset_data import ResetData
from command.assign_truck_to_route import AssignTruckToRoad
from command.create_route import CreateRoute
def commands():
    app_data = ApplicationData()
    comm = CommandFactory(app_data)
    user_app = UserApplication
    return comm, user_app


class CommandFactory_Should(unittest.TestCase):

    def test_raise_error_whenInvalidCommandName(self):
        comm_factory, user_app = commands()
        with self.assertRaises(ApplicationError):
            input_line = comm_factory.create('invalid')

    def test_registeruser_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('registeruser')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, RegisterUser)

    def test_login_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('login')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, LogIn)

    def test_show_users_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('ShowUsers')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, ShowUsers)

    def test_create_package_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('createpackage')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line.models_factory, ModelsFactory)
        self.assertIsInstance(input_line, CreatePackage)

    def test_assign_package_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('assignpackage')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, AssignPackage)

    def test_package_status_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('packagestatus')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, PackageStatus)

    def test_unassigned_packages_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('unassignedpackages')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, UnassaignedPackages)

    def test_create_route_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('CreateRoute')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line.models_factory, ModelsFactory)
        self.assertIsInstance(input_line, CreateRoute)

    def test_add_truck_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('addtruck')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line._models_factory, ModelsFactory)
        self.assertIsInstance(input_line, AddTruck)

    def test_start_command_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('start')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line.model_factory, ModelsFactory)
        self.assertIsInstance(input_line, Starting)

    def test_search_suitable_routes_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('searchsuitableroutes')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, SearchSuitableRoutes)

    def test_assign_truck_to_road_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('assigntrucktoroad')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, AssignTruckToRoad)

    def test_show_truck_status_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('showtruckstatus')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, ShowTruckStatus)

    def test_show_routes_in_progress_work_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('showroutesinprogress')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line, ShowRoutesInProgress)

    def test_reset_data_correct(self):
        comm_factory, user_app = commands()
        input_line = comm_factory.create('resetdata')

        self.assertIsInstance(input_line._app_data, ApplicationData)
        self.assertIsInstance(input_line._user_application, UserApplication)
        self.assertIsInstance(input_line._models_factory, ModelsFactory)
        self.assertIsInstance(input_line._processing_data, ProcessingData)
        self.assertIsInstance(input_line, ResetData)