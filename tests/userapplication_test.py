import unittest
from models.user import User
from core.userapplication import UserApplication
import tests.test_data as td
from errors.application_error import ApplicationError

class UserApplication_Should(unittest.TestCase):

    def test_init_setProperties(self):
        user_app = UserApplication()
        self.assertEqual(user_app.users, tuple())
        self.assertFalse(user_app.has_logged_in_user)

    def test_createsUser_WhenNotInUsers(self):
        user_app = UserApplication()
        created_user = user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME,td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
        self.assertEqual(1, len(user_app.users))
        self.assertIsInstance(created_user, User)

    def test_createUser_raisesError_whenUserAlreadyExists(self):
        with self.assertRaises(ApplicationError):
            user_app = UserApplication()
            user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME, td.VALID_LASTNAME,td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
            user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME, td.VALID_LASTNAME,td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_findsUserByUserName_whenUserExists(self):
        user_app = UserApplication()
        created_user = user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME,td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
        user = user_app.find_user_by_username(created_user.username)
        self.assertEqual(td.VALID_USERNAME, user.username)

    def test_findUserByUserName_raisesError_whenUserDoesNotExist(self):
        with self.assertRaises(ApplicationError):
            user_app = UserApplication()
            user_app.find_user_by_username(td.VALID_USERNAME)

    def test_loggedInUser_hasLoggedInuser_whenUserLoggedIn(self):
        user_app = UserApplication()
        created_user = user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME,td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
        user_app.login(created_user)
        logged_user = user_app.logged_in_user
        self.assertIsInstance(logged_user, User)

    def test_loggedInUser_raisesError_whenNoUserCreated(self):
        with self.assertRaises(ApplicationError):
            user_app = UserApplication()
            user_app.logged_in_user

    def test_loggedInUser_raisesError_whenUserLoggedOut(self):
        with self.assertRaises(ApplicationError):
            user_app = UserApplication()
            created_user = user_app.create_user(td.VALID_USERNAME, td.VALID_FIRSTNAME,td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
            user_app.login(created_user)
            user_app.logout()
            user_app.logged_in_user
    