import os
import unittest
import tests.test_data as td
from core.engine import Engine


class Engine_Should(unittest.TestCase):

    def test_init_createsInstanceOfEngine_when_validParams(self):

        engine = Engine(td.MOCK_FACTORY)

        self.assertIsInstance(engine, Engine)
        self.assertEqual(td.MOCK_FACTORY, engine._command_factory)

    def test_start_loopsUntilEndAndReturnsNone_when_ValidParams(self):

        engine = Engine(td.MOCK_FACTORY)

        with unittest.mock.patch('builtins.input', side_effect=['command1', 'command2', 'end']):
            output = engine.start()
    
        self.assertIsNone(output)

    def test_start_savesData_when_executed(self):

        engine = Engine(td.MOCK_FACTORY)
        cwd = os.path.dirname(__file__)
        file_paths = [os.path.join(cwd, "..\\data\\user_data.pickle"),
            os.path.join(cwd, "..\\data\\app_data.pickle"),
            os.path.join(cwd, "..\\data\\models_data.pickle")]
        
        with unittest.mock.patch('builtins.input', side_effect=['command1', 'command2', 'end']):
                output = engine.start()
        
        for file_path in file_paths:
            self.assertTrue(os.path.exists(file_path))

    def test__processCommand_returnsString_when_validInput(self):
        engine = Engine(td.MOCK_FACTORY)

        output = engine._process_command("showroutesinprogress")

        self.assertIsInstance(output, str)