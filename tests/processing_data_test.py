import unittest
import tests.test_data as td
from core.processing_data import ProcessingData

class ProcessingData_Should(unittest.TestCase):

    def test_init_createsInstance_when_validParams(self):
        
        output = ProcessingData(td.MOCK_FACTORY)

        self.assertEqual(td.MOCK_FACTORY, output._command_factory)
        self.assertIsInstance(output, ProcessingData)

    def test_saveData_returnsNone_when_executed(self):

        processing_data = ProcessingData(td.MOCK_FACTORY)

        output = processing_data.save_data()

        self.assertIsNone(output)

    def test_saveData_savesData_when_executed(self):
        
        mock_command_factory = td.MockCommandFactory()
        processing_data = ProcessingData(mock_command_factory)
        
        processing_data.save_data()

        mock_command_factory._app_data.save_data.assert_called_once()
        mock_command_factory._user_application.save_data.assert_called_once()
        mock_command_factory._models_factory.save_data.assert_called_once()

    def test_loadData_returnsNone_when_executed(self):

        processing_data = ProcessingData(td.MOCK_FACTORY)

        output = processing_data.load_data()

        self.assertIsNone(output)        

    def test_loadData_loadsData_when_executed(self):
        
        mock_command_factory = td.MockCommandFactory()
        processing_data = ProcessingData(mock_command_factory)
        
        processing_data.load_data()

        mock_command_factory._app_data.load_data.assert_called_once()
        mock_command_factory._user_application.load_data.assert_called_once()
        mock_command_factory._models_factory.load_data.assert_called_once()

    def test_resetData_returnsNone_when_executed(self):

        processing_data = ProcessingData(td.MOCK_FACTORY)

        output = processing_data.load_data()

        self.assertIsNone(output)