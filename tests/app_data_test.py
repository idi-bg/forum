from models.trucks import Trucks
from tests.test_data import *
import datetime
import unittest
from errors.application_error import ApplicationError
from models.package import Package
from core.application_data import ApplicationData
import os
import tests.test_data as td

class Aplication_data_Should(unittest.TestCase):

    def test_properties_work_correct(self):
        data = ApplicationData()

        self.assertIsInstance(data.trucks, list)
        self.assertIsInstance(data.routes, tuple)
        self.assertIsInstance(data.packages, tuple)

    def test_search_truck_by_route_id_worksCorrect(self):
        data = ApplicationData()

        val_truck = VALID_TRUCK
        val_truck.route_number = 5
        val_truck.available_time = datetime.datetime.strptime('2023-07-24 01:55', "%Y-%m-%d %H:%M")
        data.add_truck(val_truck)

        self.assertEqual(val_truck, data.search_truck_by_route_id(5))

    def test_add_truck_work_correct(self):
        data = ApplicationData()

        val_truck = VALID_TRUCK
        data.add_truck(val_truck)

        self.assertEqual(1, len(data.trucks))

    def test_view_information_about_trucks_correct_str(self):
        data = ApplicationData()

        val_truck = Trucks(1, 'Man', 8000, 6000)
        val_truck_2 = Trucks(2, 'Actros', 8000, 6000)
        data.add_truck(val_truck_2)
        data.add_truck(val_truck)
        val_route = VALID_ROUTE
        data.assign_truck_to_route(val_route)

        output = []
        output.append(f'Truck with id: {val_truck_2.truck_id}, capacity: {val_truck_2.capacity} and max_range: {val_truck_2.max_range} is '
                      f'assign on route {val_truck_2.route_number} and will be available'
                      f' after {val_truck_2.available_time.strftime(val_truck_2._format)}.')
        output.append(
            f'Truck with id: {val_truck.truck_id}, capacity: {val_truck.capacity} and max_range: {val_truck.max_range} is free.')
        to_print = '\n'.join(output)

        self.assertEqual(to_print, data.view_information_about_trucks())

    def test_calculate_weight_work_correct(self):
        data = ApplicationData()
        val_route = VALID_ROUTE
        package_1 = Package(1, 'Alice springs', 'Melbourne', 1000, 'Ivan', '1234567890')
        package_2 = Package(2, 'Melbourne', 'Brisbane', 4000, 'Gosho', '1234567890')
        data._packages.append(package_1)
        data._packages.append(package_2)

        self.assertEqual(4000, data.calculate_weight(val_route))

    def test_check_weight_to_add_package_returnTrue(self):
        data = ApplicationData()
        val_route = VALID_ROUTE
        val_truck = VALID_TRUCK
        time = list(val_route.stops_and_time.items())[-1][1]
        val_truck.available_time = datetime.datetime.strptime(time, '%Y-%m-%d %H:%M')
        val_truck.route_number = val_route.id
        data._routes.append(val_route)
        data.trucks.append(val_truck)

        package_1 = VALID_PACKAGE_2

        self.assertTrue(data.check_weight_to_add_package(val_route.id, package_1))


    def test_assign_truck_to_route_work_correct(self):
        data = ApplicationData()
        val_truck = VALID_TRUCK
        val_route = VALID_ROUTE
        data.trucks.append(val_truck)

        self.assertEqual(val_truck, data.assign_truck_to_route(val_route))

    def test_assign_truck_to_route_raise_error_whenNoFreeTrucks(self):
        data = ApplicationData()
        val_route = VALID_ROUTE

        with self.assertRaises(ApplicationError):
            data.assign_truck_to_route(val_route)

    def test_assign_truck_to_route_raise_error_whenNoSuitableTruck(self):
        data = ApplicationData()
        val_route = VALID_ROUTE
        val_package = Package(1, 'Alice springs', 'Melbourne', 40000, 'Ivan', '1234567890')
        val_package_2 = Package(1, 'Alice springs', 'Melbourne', 30000, 'Ivan', '1234567890')

        with self.assertRaises(ApplicationError):
            data.assign_truck_to_route(val_route)

    def test_save_data_work_correct(self):
        data = ApplicationData()
        package = VALID_PACKAGE
        route = VALID_ROUTE
        file_path = "data/app_data.pickle"

        data.save_data()

        self.assertTrue(os.path.dirname(file_path))

    def test_load_data_work_correct(self):
        data = ApplicationData()
        package = VALID_PACKAGE
        route = VALID_ROUTE
        data._packages.append(package)
        data._routes.append(route)
        file_path = "data/app_data.pickle"
        data.save_data()
        data._packages = []
        data._routes = []

        data.load_data()

        self.assertEqual(1, len(data.packages))
        self.assertEqual(1, len(data.routes))

    def test_reset_data_work_correct(self):
        data = ApplicationData()
        package = VALID_PACKAGE
        route = VALID_ROUTE
        data._packages.append(package)
        data._routes.append(route)

        data.save_data()
        data.reset_data()

        self.assertEqual(0, len(data.packages))
        self.assertEqual(0, len(data.routes))

    def test_parsedCurrentTime_classMethod_returnsCorrectDatetimeObject_when_called(self):
        
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        expected_parsed_time = datetime.datetime.strptime(current_time, "%Y-%m-%d %H:%M")        
        
        output = ApplicationData.parsed_current_time()

        self.assertIsInstance(output, datetime.datetime)       
        self.assertEqual(output, expected_parsed_time)

    def test_tryParseDateTime_parsesDateTime_when_validInput(self):

        app_data = ApplicationData()
        datetime_string = "23-FEB-2026-00"
        expected = datetime.datetime.strptime(datetime_string, "%d-%b-%Y-%H")

        output = app_data._try_parse_datetime(datetime_string)

        self.assertIsInstance(output, datetime.datetime)
        self.assertEqual(expected, output)

    def test_tryParseDateTime_raisesApplicationError_when_invalidInput(self):

        app_data = ApplicationData()
        datetime_string = "23-FEs-2026-00"
       
        with self.assertRaises(ApplicationError):
            _ = app_data._try_parse_datetime(datetime_string)

    def test_addRoute_appendsSuccessfully_when_validRoute(self):

        app_data = ApplicationData()
        route = td.VALID_ROUTE

        app_data.add_route(route)

        self.assertEqual(1, len(app_data.routes))

    def test_addRoute_appendsSuccessfully_when_moreThanOneRoutes(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        route2 = td.VALID_ROUTE_TWO

        app_data.add_route(route1)
        app_data.add_route(route2)

        self.assertEqual(2, len(app_data.routes))

    def test_searchRouteByID_returnsRoute_when_validId(self):
        
        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)
        route2 = td.VALID_ROUTE_TWO
        app_data.add_route(route2)

        output = app_data.search_route_by_id(2)

        self.assertEqual(route2, output)

    def test_searchRouteByID_raisesError_when_invalidId(self):
        
        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)
    
        with self.assertRaises(ApplicationError):
            _ = app_data.search_route_by_id(2)

    def test_calculateStopWeight_returnsZero_when_noPackagesAssigned(self):

        app_data = ApplicationData()
        route = td.VALID_ROUTE
        app_data.add_route(route)
        
        output = app_data.calculate_stop_weight("Alice Springs", 1)

        self.assertEqual(0, output)

    def test_calculateStopWeight_returnsCorrectKilos_when_PackagesAssigned(self):

        app_data = ApplicationData()
        route = td.VALID_ROUTE
        app_data.add_route(route)
        package = Package(1, 'Alice Springs', 'Melbourne', 1000, 'Ivan', '1234567890')
        app_data.add_package(package)
        app_data.assign_route_to_package(package, 1)
        
        output = app_data.calculate_stop_weight("Alice Springs", 1)

        self.assertEqual(1000, output)

    def test_calculateStopWeight_returnsZeroKg_when_PackagesDelivered(self):

        app_data = ApplicationData()
        route = td.VALID_ROUTE
        app_data.add_route(route)
        package = Package(1, 'Alice Springs', 'Melbourne', 1000, 'Ivan', '1234567890')
        app_data.add_package(package)
        app_data.assign_route_to_package(package, 1)
        
        output = app_data.calculate_stop_weight("Melbourne", 1)

        self.assertEqual(0, output)

    def test_routeExists_returnTrue_when_routeExists(self):
        
        app_data = ApplicationData()
        route = td.VALID_ROUTE
        app_data.add_route(route)

        output = app_data.route_exists('23-JUL-2023-00', ['Alice Springs', 'Adelaide', 'Melbourne', 'Sydney', 'Brisbane'])

        self.assertTrue(output)

    def test_routeExists_returnFalse_when_noRoutes(self):
        
        app_data = ApplicationData()

        output = app_data.route_exists('23-JUL-2023-00', ['Alice Springs', 'Adelaide', 'Melbourne', 'Sydney', 'Brisbane'])

        self.assertFalse(output)

    def test_routeExists_returnFalse_when_noExactSameRoute(self):
        
        app_data = ApplicationData()
        route = td.VALID_ROUTE
        app_data.add_route(route)

        output = app_data.route_exists('23-JUL-2024-00', ['Alice Springs', 'Adelaide', 'Melbourne', 'Sydney', 'Brisbane'])

        self.assertFalse(output)

    def test_getSuitableRoutes_returnsOneSuitableRoute_when_routeExists(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)
        route2 = td.VALID_ROUTE_TWO
        app_data.add_route(route2)

        output = app_data.get_suitable_routes('Adelaide', 'Brisbane')

        self.assertEqual(2, len(output))
        self.assertEqual(route1, output[0])
        self.assertEqual(route2, output[1])

    def test_getSuitableRoutes_returnsFalse_when_noRoutesInProgress(self):

        app_data = ApplicationData()

        output = app_data.get_suitable_routes('Adelaide', 'Brisbane')

        self.assertFalse(output)

    def test_getSuitableRoutes_returnsFalse_when_endLocationIsAfterStartLocation(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)

        output = app_data.get_suitable_routes('Brisbane', 'Adelaide')

        self.assertFalse(output)

    def test_getRoutesInProgress_when_routesInProgressExist(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)
        route2 = td.VALID_ROUTE_TWO
        app_data.add_route(route2)

        output = app_data.get_routes_in_progress(app_data.routes)

        self.assertEqual(2, len(output))
        self.assertIsInstance(output, list)

    def test_getRoutesInProgress_ignoresRoutesWhichReachedFinalDestination_when_validInput(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        app_data.add_route(route1)
        route2 = td.VALID_ROUTE_TWO
        app_data.add_route(route2)
        route3 = td.VALID_ROUTE_THREE_REACHED_DESTINATION
        app_data.add_route(route3)

        output = app_data.get_routes_in_progress(app_data.routes)

        self.assertEqual(2, len(output))
        self.assertIsInstance(output, list)


    def test_getRoutesInProgress_returnsEmptyList_when_noRoutes(self):

        app_data = ApplicationData()
  
        output = app_data.get_routes_in_progress(app_data.routes)

        self.assertEqual(0, len(output))
        self.assertIsInstance(output, list)

    def test_expectedCurrentStop_returnsCurrentStopAndTimeOfRoute_when_validInput(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE
        expected = {'Alice Springs': '2023-07-23 00:00'}
       
        output = app_data.expected_current_stop(route1)

        self.assertEqual(1, len(output))
        self.assertIsInstance(output, dict)
        self.assertEqual(expected, output)

    def test_expectedCurrentStop_returnsNone_when_routeCompleted(self):

        app_data = ApplicationData()
        route1 = td.VALID_ROUTE_THREE_REACHED_DESTINATION
           
        output = app_data.expected_current_stop(route1)

        self.assertIsNone(output)

    def test_searchPackageById_withMultiplePackages(self):
        
        app_data = ApplicationData()
        package_one = VALID_PACKAGE
        package_two = VALID_PACKAGE_2
        app_data.add_package(package_one)
        app_data.add_package(package_two)
        searched_package = app_data.search_package_by_id(package_one.id)
        self.assertEqual(package_one.id, searched_package.id)
        searched_package = app_data.search_package_by_id(package_two.id)
        self.assertEqual(package_two.id, searched_package.id)

    def test_searchPackageById_raisesErrorWithNoPackages(self):

        with self.assertRaises(ApplicationError):
            app_data = ApplicationData()
            app_data.search_package_by_id(VALID_PACKAGE_ID)
    
    def test_searchPackageById_raisesErrorWhenPackageNotFound(self):

        with self.assertRaises(ApplicationError):
            app_data = ApplicationData()
            package_one = VALID_PACKAGE
            package_two = VALID_PACKAGE_2
            app_data.add_package(package_one)
            app_data.add_package(package_two)
            app_data.search_package_by_id(3)
    
    def test_addPackage_addsPackage(self):
        app_data = ApplicationData()
        app_data.add_package(VALID_PACKAGE)
        packages = app_data.packages
        self.assertEqual(len(packages), 1)

    def test_addPackage_addsConsecutivesPackage(self):
        app_data = ApplicationData()
        app_data.add_package(VALID_PACKAGE)
        app_data.add_package(VALID_PACKAGE_2)
        packages = app_data.packages
        self.assertEqual(len(packages), 2)

    def test_locationsPackage_raisesErrorWhenStartEndLocationsNotInSequence(self):
        with self.assertRaises(ApplicationError):
            app_data = ApplicationData()
            route = VALID_ROUTE
            app_data.add_route(route)
            package = Package(1,'Melbourne', 'Alice springs', 1000, 'Ivan', '1234567890')
            app_data.check_locations_package(package, route.id)
    
    def test_locationsPackage_raisesErrorWhenStartEndLocationsNotiNRoute(self):
        with self.assertRaises(ApplicationError):
            app_data = ApplicationData()
            route = VALID_ROUTE
            app_data.add_route(route)
            package = Package(1,'Darwin', 'Alice springs', 1000, 'Ivan', '1234567890')
            app_data.check_locations_package(package, route.id)

    def test_assignRouteToPackage_assignsCorrectAttributesToPackage(self):
        app_data = ApplicationData()
        route = VALID_ROUTE
        app_data.add_route(route)
        package = VALID_PACKAGE_2
        package = app_data.assign_route_to_package(package, route.id)
        self.assertEqual(package.route_number, route.id)
        self.assertEqual(package.depart_time, route.stops_and_time[package.start_location])
        self.assertEqual(package.arrival_time, route.stops_and_time[package.end_location])
        self.assertEqual(package.status, 'Assigned')

    def test_showPackageInfo_ReturnCorrectlyFormattedOutputWhenNotAssigned(self):
        app_data = ApplicationData()
        package = Package(VALID_PACKAGE_ID, VALID_PACKAGE_START_LOCATION, VALID_PACKAGE_END_LOCATION, 
                          VALID_PACKAGE_WEIGHT, VALID_PACKAGE_NAME, VALID_PACKAGE_PHONE)
        app_data.add_package(package)
        expected = f'''Package {VALID_PACKAGE_ID}, which belongs to {VALID_PACKAGE_NAME}(phone number {VALID_PACKAGE_PHONE}), has status [Not assigned].'''
        actual = app_data.show_package_info(package.id)
        self.assertEqual(expected, actual)

    def test_showPackageInfo_ReturnCorrectlyFormattedOutputWhenAssigned(self):
        app_data = ApplicationData()
        package = Package(2, 'Melbourne', 'Brisbane', 4000, 'Gosho', '1234567890')
        route = VALID_ROUTE
        app_data.add_route(route)
        app_data.add_package(package)
        app_data.assign_route_to_package(package, route.id)
        expected = f'''Package {package.id}, which belongs to {package.client_name}(phone number {package.client_phone}), has status [Assigned].'''
        actual = app_data.show_package_info(package.id)
        self.assertEqual(expected, actual)
        
    def test_showUnassignedPackagesWithMultiplePackages(self):
        app_data = ApplicationData()
        package = VALID_PACKAGE_2
        packagetwo = Package(1, 'Alice springs', 'Melbourne', 1000, 'Ivan', '1234567890')
        route = VALID_ROUTE
        app_data.add_route(route)
        app_data.add_package(package)
        app_data.add_package(packagetwo)
        app_data.assign_route_to_package(package, route.id)
        expected = f'Package with id {packagetwo.id} is at location {packagetwo.start_location}.'
        actual = app_data.show_unassaigned_packages()
        self.assertEqual(expected, actual)

    def test_showUnassignedPackages_WhenNoUnassignedPackages(self):
       
        app_data = ApplicationData()
        expected = 'Currently there are no unassigned packages.'
        actual = app_data.show_unassaigned_packages()
        self.assertEqual(expected, actual)

    def test_managerView (self):

        app_data = ApplicationData()
        expected = 'No routes in progress.'
        actual = app_data.manager_view()
        self.assertEqual(expected, actual)

        
