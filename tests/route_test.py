import unittest
import datetime
import tests.test_data as td
from models.route import Route
from errors.application_error import ApplicationError


class Route_Should(unittest.TestCase):

    def test_init_CreatesInstanceOfRoute_when_validParams(self):
        
        route = td.VALID_ROUTE
        
        self.assertIsInstance(route, Route)    
        self.assertEqual(1, route.id)
        self.assertEqual(td.VALID_DEPARTURE_TIME, route.departure_time)
        self.assertEqual(td.VALID_ROUTE_STOPS, route.route_stops)
        self.assertEqual(td.VALID_STOPS_AND_TIME, route.stops_and_time)

    def test_init_concatenateIfAliceSpringsExist_when_ValidInput(self):

        route = Route(td.VALID_ROUTE_ID, td.VALID_DEPARTURE_TIME, ['alice', 'springs', 'Adelaide'])

        self.assertEqual(2, len(route.route_stops))
    
    def test_init_vaildateLocations_raiseApplicationError_when_InvalidLocation(self):

        with self.assertRaises(ApplicationError):
            route = Route(td.VALID_ROUTE_ID, td.VALID_DEPARTURE_TIME, td.INVALID_ROUTE_STOPS)

    def test_init_vaildateLocations_raiseApplicationError_when_LessThanTwoLocations(self):

        with self.assertRaises(ApplicationError):
            route = Route(td.VALID_ROUTE_ID, td.VALID_DEPARTURE_TIME, ['Alice Springs'])

    def test_init_vaildateLocations_raiseApplicationError_when_duplicateLocations(self):

        with self.assertRaises(ApplicationError):
            route = Route(td.VALID_ROUTE_ID, td.VALID_DEPARTURE_TIME, ['Alice Springs', 'Alice Springs', 'Brisbane'])

    def test_avgSpeed_classAttributeExists(self):

        self.assertEqual(td.AVG_SPEED, Route.AVG_SPEED)
    
    def test_id_readOnlyPropertyExist(self):

        route = td.VALID_ROUTE

        self.assertEqual(1, route.id)
        self.assertIsInstance(route.id, int)

    def test_departureTime_readOnlyPropertyExist(self):

        route = td.VALID_ROUTE

        self.assertEqual(td.VALID_DEPARTURE_TIME, route.departure_time)
        self.assertIsInstance(route.departure_time, datetime.datetime)

    def test_routeStops_readOnlyPropertyExist(self):

        route = td.VALID_ROUTE

        self.assertEqual(td.VALID_ROUTE_STOPS, route.route_stops)
        self.assertIsInstance(route.route_stops, tuple)

    def test_totalTime_readOnlyPropertyExist(self):

        route = td.VALID_ROUTE

        self.assertEqual(td.VALID_TOTAL_TIME, route.total_time)
        self.assertIsInstance(route.total_time, int)

    def test_totalDistance_readOnlyPropertyExist(self):

        route = td.VALID_ROUTE

        self.assertEqual(td.VALID_TOTAL_DISTANCE, route.total_distance)
        self.assertIsInstance(route.total_distance, int)

    def test_routeCreation_returnsDictWithStopsAndTime_when_validPrams(self):
        
        route = td.VALID_ROUTE

        route_stops_and_time = route.route_creation(td.VALID_DEPARTURE_TIME, td.VALID_ROUTE_STOPS)

        self.assertEqual(td.VALID_STOPS_AND_TIME, route_stops_and_time)
        self.assertIsInstance(route_stops_and_time, dict)

    def test_toString_returnsFormatterString_when_validParams(self):
        
        route = Route(td.VALID_ROUTE_ID, td.VALID_DEPARTURE_TIME, ['alice', 'springs', 'Adelaide'])
        expected = f'{str(route)}\n'
        expected += '\n'.join([f'  City: {key} : Arrival time {value}' for key, value in route.stops_and_time.items()])
        
        output = route.to_string()
        
        self.assertEqual(expected, output)

    def test__str__overridedDunder_returnsCorrectlyFormattedStr_when_validParams(self):

        route = td.VALID_ROUTE
        expected = f'Route # {route.id} -> '
        expected += ' -> '.join([f'{key} ({value})' for key, value in route.stops_and_time.items()])
    
        output = str(route)

        self.assertIsInstance(output, str)
        self.assertEqual(expected, output)

    def test__contains__overridedDunder_returnTrueIfLocationInRouteInstance(self):
        
        route = td.VALID_ROUTE

        output = 'Alice Springs'in route

        self.assertTrue(output)

    def test__len__overridedDunder_returnsLenCorrectly(self):
        
        route = td.VALID_ROUTE

        output = len(route)

        self.assertEqual(5, output)