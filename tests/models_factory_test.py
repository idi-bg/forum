import unittest
from core.models_factory import ModelsFactory
import tests.test_data as td
from models.package import Package
from models.trucks import Trucks
from models.route import Route

class ModelsFactory_Should(unittest.TestCase):

    def test_create_package(self):
        models_factory = ModelsFactory()
        
        package = models_factory.create_package(td.VALID_PACKAGE_START_LOCATION, td.VALID_PACKAGE_END_LOCATION, 
                                                td.VALID_PACKAGE_WEIGHT, td.VALID_PACKAGE_NAME, td.VALID_PACKAGE_PHONE)
        self.assertIsInstance(package, Package)
        self.assertEqual(package.id, 1)
        models_factory.reset_data()

    def test_create_consecutivesPackages(self):
        models_factory = ModelsFactory()
        package = models_factory.create_package(td.VALID_PACKAGE_START_LOCATION, td.VALID_PACKAGE_END_LOCATION, 
                                                td.VALID_PACKAGE_WEIGHT, td.VALID_PACKAGE_NAME, td.VALID_PACKAGE_PHONE)
        packagetwo = models_factory.create_package(td.VALID_PACKAGE_START_LOCATION, td.VALID_PACKAGE_END_LOCATION, 
                                                td.VALID_PACKAGE_WEIGHT, td.VALID_PACKAGE_NAME, td.VALID_PACKAGE_PHONE)
        self.assertIsInstance(package, Package)
        self.assertEqual(package.id, 1)
        self.assertIsInstance(packagetwo, Package)
        self.assertEqual(packagetwo.id, 2)
        models_factory.reset_data()

    def test_create_route(self):
        models_factory = ModelsFactory()
        route = models_factory.create_route(td.VALID_DEPARTURE_TIME,td.VALID_ROUTE_STOPS)
        self.assertIsInstance(route, Route)
        self.assertEqual(route.id, 1)
        models_factory.reset_data()

    def test_create_consecutiveRoutes(self):
        models_factory = ModelsFactory()
        route = models_factory.create_route(td.VALID_DEPARTURE_TIME,td.VALID_ROUTE_STOPS)
        routetwo = models_factory.create_route(td.VALID_DEPARTURE_TIME,td.VALID_ROUTE_STOPS)
        self.assertIsInstance(route, Route)
        self.assertEqual(route.id, 1)
        self.assertIsInstance(routetwo, Route)
        self.assertEqual(routetwo.id, 2)
        models_factory.reset_data()
    
    def test_create_truck(self):
        models_factory = ModelsFactory()
        truck = models_factory.create_truck(td.VALID_TRUCK_NAME, td.VALID_TRUCK_CAPACITY, td.VALID_TRUCK_RANGE)
        self.assertIsInstance(truck, Trucks)
        self.assertEqual(truck.truck_id, 1001)
        models_factory.reset_data()

    def test_create_consecutiveTrucks(self):
        models_factory = ModelsFactory()
        truck = models_factory.create_truck(td.VALID_TRUCK_NAME, td.VALID_TRUCK_CAPACITY, td.VALID_TRUCK_RANGE)
        trucktwo = models_factory.create_truck(td.VALID_TRUCK_NAME, td.VALID_TRUCK_CAPACITY, td.VALID_TRUCK_RANGE)
        self.assertIsInstance(truck, Trucks)
        self.assertEqual(truck.truck_id, 1001)
        self.assertIsInstance(trucktwo, Trucks)
        self.assertEqual(trucktwo.truck_id, 1002)
        models_factory.reset_data()
    