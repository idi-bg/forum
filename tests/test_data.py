from unittest.mock import Mock, MagicMock
from models.route import Route
import datetime
from models.trucks import Trucks
from models.constants.truck_status_params import *
from models.package import Package
from models.constants.user_roles import UserRoles
import core.command_factory

AVG_SPEED = 87
VALID_DEPARTURE_TIME = datetime.datetime.strptime('23-JUL-2023-00', '%d-%b-%Y-%H')
DEPARTURE_TIME_IN_THE_PAST = datetime.datetime.strptime('23-JUL-2022-00', '%d-%b-%Y-%H')
VALID_ROUTE_ID = 1
VALID_ROUTE_STOPS = tuple(['Alice Springs', 'Adelaide', 'Melbourne', 'Sydney', 'Brisbane'])
VALID_STOPS_AND_TIME = {'Alice Springs': '2023-07-23 00:00', 'Adelaide': '2023-07-23 17:35', 'Melbourne': '2023-07-24 01:55', 'Sydney': '2023-07-24 12:00', 'Brisbane': '2023-07-24 22:26'}
VALID_TOTAL_DISTANCE = 4041
VALID_TOTAL_TIME = 46

VALID_ROUTE = Route(VALID_ROUTE_ID, VALID_DEPARTURE_TIME, VALID_ROUTE_STOPS )
VALID_ROUTE_TWO = Route(2, VALID_DEPARTURE_TIME, VALID_ROUTE_STOPS )
VALID_ROUTE_THREE_REACHED_DESTINATION = Route(3, DEPARTURE_TIME_IN_THE_PAST, VALID_ROUTE_STOPS)

INVALID_ROUTE_STOPS = ['Alice Springs', 'Adelaide', 'Melbourne', 'Sydney', 'Bris']


VALID_TRUCK_ID = 1
VALID_TRUCK_NAME = 'MAN'
VALID_TRUCK_CAPACITY = 10000
VALID_TRUCK_RANGE = 5000
VAlID_TRUCK_STATUS = TruckStatus.FREE
VALID_ROUTE_NUMBER = 2

VALID_TRUCK = Trucks(VALID_TRUCK_ID, VALID_TRUCK_NAME, VALID_TRUCK_CAPACITY, VALID_TRUCK_RANGE)

MOCK_FACTORY = Mock()
MOCK_COMMAND = Mock()
MOCK_COMMAND.execute.return_value = "some string value"
MOCK_FACTORY.create.return_value = MOCK_COMMAND

VALID_PACKAGE = Package(1, 'Alice springs', 'Melbourne', 1000, 'Ivan', '1234567890')
VALID_PACKAGE_2 = Package(2, 'Melbourne', 'Brisbane', 4000, 'Gosho', '1234567890')

#Data on user
VALID_USERNAME = 'TestUsername'
VALID_FIRSTNAME = 'TestFirstname'
VALID_LASTNAME = 'TestLastname'
VALID_PASSWORD = 'TestPassword'
MANAGER_ROLE = UserRoles.MANAGER
EMPLOYEE_ROLE = UserRoles.EMPLOYEE
SUPERVISOR_ROLE = UserRoles.SUPERVISOR

#Data on package
VALID_PACKAGE_ID = 1
VALID_PACKAGE_START_LOCATION = 'Alice springs'
VALID_PACKAGE_END_LOCATION = 'Melbourne'
VALID_PACKAGE_WEIGHT = 1000
VALID_PACKAGE_NAME = 'Ivan'
VALID_PACKAGE_PHONE = '1234567890'

class MockCommandFactory(core.command_factory.CommandFactory):
    def __init__(self):
        self._app_data = MagicMock()
        self._user_application = MagicMock()
        self._models_factory = MagicMock()
