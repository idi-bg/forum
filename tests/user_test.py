import unittest
from models.constants.user_roles import UserRoles
from models.user import User
import tests.test_data as td

class User_Should(unittest.TestCase):
    
    def test_init_setProperties(self):
        
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

        
        self.assertEqual(td.VALID_USERNAME, user.username)
        self.assertEqual(td.VALID_FIRSTNAME, user.firstname)
        self.assertEqual(td.VALID_LASTNAME, user.lastname)
        self.assertEqual(td.VALID_PASSWORD, user.password)
        self.assertEqual(td.EMPLOYEE_ROLE, user.user_role)

    def test_init_raiseError_usernameTooShort(self):
        with self.assertRaises(ValueError):
            User('a', td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_usernameTooLong(self):
        with self.assertRaises(ValueError):
            User('a' * 21, td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_usernameInvalidSymbol(self):
        with self.assertRaises(ValueError):
            User('testusern@me', td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_passwordTooShort(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, 'asdf', td.EMPLOYEE_ROLE)

    def test_init_raiseError_passwordTooLong(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, 'a' * 31, td.EMPLOYEE_ROLE)

    def test_init_raiseError_passwordInvalidSymbol(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                 td.VALID_LASTNAME, 'testpa$$word', td.EMPLOYEE_ROLE)

    def test_init_raiseError_firstnameTooLong(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, 'a',
                 td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_firstnameTooLong(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, 'a' * 21,
                 td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_lastnameTooLong(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                 'a', td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

    def test_init_raiseError_lastnameTooLong(self):
        with self.assertRaises(ValueError):
            User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                 'a' * 21, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
            
    def test_ismanager_returnsTrue_whenManager(self):
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.MANAGER_ROLE)
        
        self.assertTrue(user.is_manager)

    def test_ismanager_returnsFalse_whenNotManager(self):
        
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

        self.assertFalse(user.is_manager)

    def test_issupervisor_returnsTrue_whenSupervisor(self):
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.SUPERVISOR_ROLE)
        
        self.assertTrue(user.is_supervisor)

    def test_issupervisor_returnsFalse_whenNotSupervisor(self):
        
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)

        self.assertFalse(user.is_supervisor)
    
    def test_str_correctlyFormattedOutput(self):
        
        user = User(td.VALID_USERNAME, td.VALID_FIRSTNAME,
                    td.VALID_LASTNAME, td.VALID_PASSWORD, td.EMPLOYEE_ROLE)
        expected = f'Username: {td.VALID_USERNAME}, FullName: {td.VALID_FIRSTNAME} {td.VALID_LASTNAME}, Role: {td.EMPLOYEE_ROLE}'
        actual = user.__str__()

        self.assertEqual(expected, actual)