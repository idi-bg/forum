from models.trucks import Trucks
from tests.test_data import *
import datetime
import unittest
from errors.application_error import ApplicationError


class Trucks_Should(unittest.TestCase):
    def test_create_truck_successfully(self):
        current = VALID_TRUCK

        self.assertEqual(VALID_TRUCK_ID, current.truck_id)
        self.assertEqual(VALID_TRUCK_NAME, current.truck_name)
        self.assertEqual(VALID_TRUCK_CAPACITY, current.capacity)
        self.assertEqual(VALID_TRUCK_RANGE, current.max_range)
        self.assertEqual(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
                         current.available_time.strftime("%Y-%m-%d %H:%M"))
        self.assertEqual('Free', current.status)
        self.assertEqual(None, current.route_number)

    def test_raise_error_whenCapacityIsNegative(self):
        with self.assertRaises(ApplicationError):
            current = Trucks(VALID_TRUCK_ID, VALID_TRUCK_NAME, -1, VALID_TRUCK_RANGE)

    def test_raise_error_whenRangeIsNegative(self):
        with self.assertRaises(ApplicationError):
            current = Trucks(VALID_TRUCK_ID, VALID_TRUCK_NAME, VALID_TRUCK_CAPACITY, -1)

    def test_available_time_raise_Error_whenTimeInThePast(self):
        current = VALID_TRUCK

        with self.assertRaises(ValueError):
            current.available_time = datetime.datetime.strptime('2022-07-24 01:55', "%Y-%m-%d %H:%M")

    def test_truck_str_method_when_theTruckIsFree(self):
        current = VALID_TRUCK

        to_print = f'Truck with id: {VALID_TRUCK_ID}, capacity: {VALID_TRUCK_CAPACITY} and max_range: {VALID_TRUCK_RANGE} is free.'

        self.assertEqual(to_print, f"{current}")

    def test_truck_str_method_when_theTruckIsNotFree(self):
        current = VALID_TRUCK
        current.status = TruckStatus.ON_ROAD
        current.route_number = VALID_ROUTE_NUMBER

        to_print = f'Truck with id: {VALID_TRUCK_ID}, capacity: {VALID_TRUCK_CAPACITY} and max_range: {VALID_TRUCK_RANGE} is ' \
                   f'assign on route {VALID_ROUTE_NUMBER} and will be available' \
                   f' after {current.available_time.strftime("%Y-%m-%d %H:%M")}.'

        self.assertEqual(to_print, f"{current}")
