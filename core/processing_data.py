from __future__ import annotations
from os import path, remove
import core.command_factory

class ProcessingData:

    def __init__(self, command_factory: core.command_factory.CommandFactory):
        self._command_factory = command_factory
   
    def save_data(self):

        self._command_factory._app_data.save_data()
        self._command_factory._user_application.save_data()
        self._command_factory._models_factory.save_data()

    def load_data(self):

        self._command_factory._app_data.load_data()
        self._command_factory._user_application.load_data()
        self._command_factory._models_factory.load_data()

    def reset_data(self):

        cwd = path.dirname(__file__)
        file_paths = [
            path.join(cwd, "..\\data\\user_data.pickle"),
            path.join(cwd, "..\\data\\app_data.pickle"),
            path.join(cwd, "..\\data\\models_data.pickle")
        ]
        for file_path in file_paths:
            if path.isfile(file_path):
                remove(file_path)

        self._command_factory._app_data.reset_data()
        self._command_factory._user_application.reset_data()
        self._command_factory._models_factory.reset_data()