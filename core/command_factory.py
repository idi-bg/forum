from errors.application_error import ApplicationError
from core.application_data import ApplicationData
from core.models_factory import ModelsFactory
from core.processing_data import ProcessingData
from core.userapplication import UserApplication
from command.add_truck import AddTruck
from command.starting_command import Starting
from command.create_package import CreatePackage
from command.search_suitable_routes import SearchSuitableRoutes
from command.assign_route_to_package import AssignPackage
from command.show_truck_status import ShowTruckStatus
from command.show_package_status import PackageStatus
from command.show_routes_in_progress import ShowRoutesInProgress
from command.register_user import RegisterUser
from command.login import LogIn
from command.logout import LogOut
from command.show_users import ShowUsers
from command.show_unassigned_packages import UnassaignedPackages
from command.reset_data import ResetData
from command.assign_truck_to_route import AssignTruckToRoad
from command.create_route import CreateRoute


class CommandFactory:
    def __init__(self, data: ApplicationData):
        self._app_data = data
        self._user_application = UserApplication()
        self._models_factory = ModelsFactory()
        self._processing_data = ProcessingData(self)

    def create(self, cmd: str):
        if cmd.lower() == "registeruser":
            return RegisterUser(self._app_data, self._user_application)
        if cmd.lower() == "login":
            return LogIn(self._app_data, self._user_application)
        if cmd.lower() == "logout":
            return LogOut(self._app_data, self._user_application)
        if cmd.lower() == "showusers":
            return ShowUsers(self._app_data, self._user_application)
        if cmd.lower() == "createpackage":
            return CreatePackage(self._app_data,self._user_application, self._models_factory)
        if cmd.lower() == "assignpackage":
            return AssignPackage(self._app_data, self._user_application)
        if cmd.lower() == "packagestatus":
            return PackageStatus(self._app_data, self._user_application)
        if cmd.lower() == "unassignedpackages":
            return UnassaignedPackages(self._app_data, self._user_application)
        if cmd.lower() == "createroute":
            return CreateRoute(self._app_data, self._user_application, self._models_factory)
        if cmd.lower() == "addtruck":
            return AddTruck(self._app_data, self._models_factory, self._user_application)
        if cmd.lower() == "start":
            return Starting(self._app_data,self._user_application, self._models_factory)
        if cmd.lower() == "searchsuitableroutes":
            return SearchSuitableRoutes(self._app_data, self._user_application)
        if cmd.lower() == "assigntrucktoroad":
            return AssignTruckToRoad(self._app_data, self._user_application)
        if cmd.lower() == "showtruckstatus":
            return ShowTruckStatus(self._app_data, self._user_application)
        if cmd.lower() == "showroutesinprogress":
            return ShowRoutesInProgress(self._app_data, self._user_application)
        if cmd.lower() == "resetdata":
            return ResetData(self._processing_data, self._app_data,self._user_application, self._models_factory)

        raise ApplicationError(f'Invalid command name: {cmd}!')
