from models.trucks import Trucks
from models.route import Route
from models.package import Package
import datetime
import os
import pickle


class ModelsFactory():

    _route_id = 1
    _package_id = 1
    _truck_id = 1001

    def __init__(self):
        pass

    def create_package(self, start_location: str, end_location: str, weight: int, client_name: str, client_phone: str):

        package = Package(ModelsFactory._package_id, start_location, end_location, weight, client_name, client_phone)
        ModelsFactory._package_id += 1
        return package

    def create_truck(self, name: str, capacity: int, max_range: int):

        new = Trucks(ModelsFactory._truck_id, name, capacity, max_range)
        ModelsFactory._truck_id += 1
        return new

    def create_route(self, departure_time: datetime, route_locations: list[str]) -> Route:
        
        route = Route(ModelsFactory._route_id, departure_time, route_locations)
        ModelsFactory._route_id += 1
        return route

    def save_data(self):

        data = {
            "route_id": ModelsFactory._route_id,
            "package_id": ModelsFactory._package_id,
            "truck_id": ModelsFactory._truck_id,
        }

        file_path = "data/models_data.pickle"
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        with open("data/models_data.pickle", "wb") as file:
            pickle.dump(data, file)

    def load_data(self):

        if os.path.isfile("data/models_data.pickle"):
            with open("data/models_data.pickle", "rb") as file:
                data = pickle.load(file)
                ModelsFactory._package_id = data["route_id"]
                ModelsFactory._route_id = data["route_id"]
                ModelsFactory._truck_id = data["truck_id"]

    def reset_data(self):

        ModelsFactory._package_id = 1
        ModelsFactory._route_id = 1
        ModelsFactory._truck_id = 1001