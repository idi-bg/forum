from core.command_factory import CommandFactory
from errors.application_error import ApplicationError


class Engine:
    def __init__(self, factory: CommandFactory):
        self._command_factory = factory

    def start(self):
        self._command_factory._processing_data.load_data()
        print(self._process_command('start'))


        while True:
            self._command_factory._processing_data.save_data()
            try:
                input_line = input()
                if input_line.lower() == 'end':
                    break

                output = self._process_command(input_line)
                print(output)
            except ValueError as err:
                print(err.args[0])
            except ApplicationError as err:
                print(err.args[0])                

    def _process_command(self, input_line):
        cmd_name, *params = input_line.split()

        return self._command_factory.create(cmd_name).execute(params)