import os
import pickle
from models.user import User
from errors.application_error import ApplicationError


class UserApplication:

    def __init__(self):
        self._users = []
        self._logged_user = None

    @property
    def users(self):
        return tuple(self._users)

    def create_user(self, username, firstname, lastname, password, user_role) -> User:
        if len([u for u in self._users if u.username == username]) > 0:
            raise ApplicationError(
                f'User {username} already exist. Choose a different username!')

        user = User(username, firstname, lastname, password, user_role)
        self._users.append(user)
        return user
    
    def find_user_by_username(self, username: str) -> User:
        filtered = [user for user in self._users if user.username == username]
        if filtered == []:
            raise ApplicationError(f'There is no user with username {username}!')

        return filtered[0]
    
    @property
    def logged_in_user(self):
        if self.has_logged_in_user:
            return self._logged_user
        else:
            raise ApplicationError('There is no logged in user.')

    @property
    def has_logged_in_user(self):
        return self._logged_user is not None

    def login(self, user: User):
        self._logged_user = user

    def logout(self):
        self._logged_user = None

    def save_data(self):
        data = {
            "users": self._users,
            "logged_user": self._logged_user 
        }

        file_path = "data/user_data.pickle"
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        with open("data/user_data.pickle", "wb") as file:
            pickle.dump(data, file)

    def load_data(self):
        if os.path.isfile("data/user_data.pickle"):
            with open("data/user_data.pickle", "rb") as file:
                data = pickle.load(file)
                self._users = data["users"]
                self._logged_user = data["logged_user"]

    def reset_data(self):
        self._users = []
        self._logged_user = None