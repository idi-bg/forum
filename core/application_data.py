import os
import pickle
import datetime
from models.route import Route
from models.package import Package
from models.trucks import Trucks
from errors.application_error import ApplicationError
from models.constants.truck_status_params import TruckStatus


class ApplicationData():

    def __init__(self):

        self._routes: list[Route] = []
        self._trucks: list[Trucks] = []
        self._packages: list[Package] = []

    @classmethod
    def parsed_current_time(cls) -> datetime:
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        parsed_current_time = datetime.datetime.strptime(current_time, "%Y-%m-%d %H:%M")
        return parsed_current_time

    @property
    def routes(self) -> tuple:
        return tuple(self._routes)

    @property
    def packages(self):
        return tuple(self._packages)

    @property
    def trucks(self):
        return self._trucks
    
    def _try_parse_datetime(self, s):
        date_format = '%d-%b-%Y-%H'
        s = s.upper()
        try:
            parsed_date = datetime.datetime.strptime(s, date_format)
            return parsed_date
        except ValueError:
            raise ApplicationError(
                'Invalid value for date and time. Should be in format: DD-MMM-YYYY-HH and within range: 00 to 23.')
    
    def search_package_by_id(self, package_id: int) -> Package:
        """Returns package based on it's id number."""
        for package in self.packages:
            if package.id == package_id:
                return package
        raise ApplicationError(f"Package with id {package_id} not found.")

    def search_route_by_id(self, route_id: int) -> Route:
        """Returns route based on it's id number."""
        for route in self.routes:
            if route.id == route_id:
                return route
        raise ApplicationError(f"Road with id {route_id} not found.")
    
    def search_truck_by_route_id(self, route_id: int) -> Trucks:
        """Returns truck based on it's id number."""
        self.update_truck_status()
        for truck in self.trucks:
            if truck.route_number == route_id:
                return truck

    def add_package(self, package: Package):
        """Appends package to the list of unassigned packages."""
        self._packages.append(package)

    def add_route(self, route: Route):
        """Appends route to the list of routes."""
        self._routes.append(route)

    def check_locations_package(self, package, route_id):
        """Checks if the locations of the route are suitable for the locations of the package ."""
        route = self.search_route_by_id(route_id)
        check_for_locations = package.start_location in route.route_stops and package.end_location in route.route_stops
        if  check_for_locations:
            if route.route_stops.index(package.start_location) > route.route_stops.index(package.end_location):
                raise ApplicationError(f'This sequence of the package locations is not suitable for route {route_id}!')

        else:
            raise ApplicationError(f'This package is not suitable for route {route_id}!')

    def assign_route_to_package(self, package, route_id) -> Package:
        """Updates package attributes and by this means links package with the route."""
        route = self.search_route_by_id(route_id)
        package.route_number = route_id
        package.depart_time = route.stops_and_time[package.start_location]
        package.arrival_time = route.stops_and_time[package.end_location]
        package.change_status()

        return package

    def show_package_info(self, package_id) -> str:
        """Returns package info based on package id."""
        package = self.search_package_by_id(package_id)
        if package.status == 'Assigned':
           self.update_package_status(package)
        return package.__str__()
    
    def show_unassaigned_packages(self) -> str:
        """Returns info about all unassigned packages."""
        output = []
        unassigned_packages = [package for package in self.packages if package.status == 'Not assigned']
        for package in unassigned_packages:
            output.append(f'Package with id {package.id} is at location {package.start_location}.')
        if len(output) != 0:
            return '\n'.join(output)
        else:
            return 'Currently there are no unassigned packages.'
    
    def update_package_status(self, package) -> None:
        """Updates package status based on the arrival time of the package."""
        parsed_arrival_time = datetime.datetime.strptime(package.arrival_time, "%Y-%m-%d %H:%M")
        if parsed_arrival_time <= ApplicationData.parsed_current_time():
            package.change_status()
            

    def add_truck(self, truck: Trucks) -> None:
        """Appends truck to the list of company's trucks."""
        self._trucks.append(truck)

    def update_truck_status(self) -> None:
        """Update the status of all trucks"""
        for truck in self._trucks:
            if truck.available_time < datetime.datetime.now():
                truck.status = TruckStatus.FREE
                truck.route_number = None

    def view_information_about_trucks(self) -> str:
        """Return current information of all trucks"""
        self.update_truck_status()
        output = []
        for truck in self.trucks:
            output.append(f'{truck}')
        return '\n'.join(output)
    
    def calculate_weight(self, route: Route) -> int:
        """Calculate the max weight of all packages assigned to a current route"""
        current_route = route
        current_weight = 0
        current_max_weight = 0
        for city in current_route.route_stops[0:-2]:
            city_index = current_route.route_stops.index(city)
            for package in self.packages:
                if package.start_location == city and package.status == 'Not assigned' and \
                        package.end_location in current_route.route_stops[city_index + 1:]:
                    current_weight += package.weight
                elif package.start_location in current_route.route_stops[:city_index] and\
                        package.status == 'Not assigned' and package.end_location == city:
                    current_weight -= package.weight
            if current_max_weight < current_weight:
                current_max_weight = current_weight

        return current_max_weight

    def check_weight_to_add_package(self, route_id: int, package: Package) -> bool:
        """Returns True if there is available capacity to add package to route. False if otherwise."""
        current_truck = self.search_truck_by_route_id(route_id)
        start_city_kg = self.calculate_stop_weight(package.start_location, route_id)
        if current_truck.capacity - start_city_kg >= package.weight:
            return True
        return False

    def calculate_stop_weight(self, city, route_id) -> int:
        """Returns the current weight of the given location from certain route."""
        route = self.search_route_by_id(route_id)
        route_stop_kg = 0
        route_stop_index = route.route_stops.index(city)
        for package in self.packages:
            if package.route_number == route_id:
                if route.route_stops.index(package.start_location) <= route_stop_index < route.route_stops.index(
                        package.end_location):
                    route_stop_kg += package.weight
        return route_stop_kg

    def assign_truck_to_route(self, route: Route) -> Trucks:
        """Assign a truck with min capacity to a route"""
        current_route = route
        max_weight_of_packages = self.calculate_weight(current_route)
        self.update_truck_status()
        if any(truck.status == TruckStatus.FREE for truck in self.trucks):
            self._trucks.sort(key=lambda t: t.capacity)
            truck_to_assign = None
            for truck in self._trucks:
                if truck.capacity > max_weight_of_packages and truck.status == TruckStatus.FREE and \
                        truck.max_range > current_route.total_distance:
                    truck.status = TruckStatus.ON_ROAD
                    truck.route_number = current_route.id
                    last_value = list(current_route.stops_and_time.values())[-1]
                    truck.available_time = datetime.datetime.strptime(last_value, "%Y-%m-%d %H:%M")
                    truck_to_assign = truck
                    break
            if truck_to_assign is None:
                raise ApplicationError(f"Can't find a suitable truck to assign.")
            return truck_to_assign
        raise ApplicationError("There are no free truck now.")

    def route_exists(self, departure_time, route_stops) -> bool:
        """Returns True if route with exact same departure time and route stops exists."""
        capitalized_route_stops = tuple([city.title() for city in route_stops])
        departure_time = self._try_parse_datetime(
            departure_time)
        existing_potential_dublicate_routes = []

        for route in self.routes:
            if route.departure_time == departure_time:
                existing_potential_dublicate_routes.append(route.route_stops)

        if len(existing_potential_dublicate_routes) == 0:
            return False

        for potential_route in existing_potential_dublicate_routes:
            if capitalized_route_stops == potential_route:
                return True
        else:
            return False

    def get_suitable_routes(self, package_start_location: str, package_end_location: str) -> list:
        """Returns list of suitable routes based on package's start and end locations."""
        package_start, package_end = package_start_location.title(), package_end_location.title()
        start_index, end_index = None, None
        routes_in_progress = self.get_routes_in_progress(self._routes)
        suitable_routes = []
        for route in routes_in_progress:
            if package_start in route.route_stops and package_end in route.route_stops:
                start_index = route.route_stops.index(package_start)
                end_index = route.route_stops.index(package_end)
                start_city_time = route.stops_and_time.get(package_start)
                parsed_start_city_time = datetime.datetime.strptime(start_city_time, '%Y-%m-%d %H:%M')
                
                if start_index < end_index and parsed_start_city_time > ApplicationData.parsed_current_time():
                    suitable_routes.append(route)

        return suitable_routes

    def get_routes_in_progress(self, routes: tuple) -> list:
        """Filters only the routes in progress (not reached final destination)."""
        routes_in_progress = []
        for route in routes:
            end_city_time = list(route.stops_and_time.items())[-1][1]
            parsed_end_city_time = datetime.datetime.strptime(end_city_time, '%Y-%m-%d %H:%M')
            if parsed_end_city_time > ApplicationData.parsed_current_time():
                routes_in_progress.append(route)
        return routes_in_progress

    def expected_current_stop(self, route: Route) -> dict:
        """Returns the expected route's current stop and arrival time."""
        for key, value in route.stops_and_time.items():
            parsed_start_city_time = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M')
            if parsed_start_city_time > ApplicationData.parsed_current_time():
                return {key: value}

    def manager_view(self) -> str:
        """Returns routes in progress and each route's next stop, locations and current weight of truck. Accessible for Managers only."""
        output = []
        routes_in_progres = self.get_routes_in_progress(self.routes)
        if len(routes_in_progres) == 0:
            return 'No routes in progress.'
        for route in routes_in_progres:
            current_stop = self.expected_current_stop(route)
            stops_and_weight = [f'Route #{route.id} stops and weight:']
            next_stop = f"\nNext stop for route #{route.id} is {list(current_stop.keys())[0]} : {list(current_stop.values())[0]}"
            for route_stop in route.route_stops:
                stops_and_weight.append(f"({route_stop}: {self.calculate_stop_weight(route_stop, route.id)} kg)")
            route_info = ' '.join(stops_and_weight) + next_stop
            output.append(route_info)
        return '\n'.join(output)

    def save_data(self):
        data = {
            "routes": self._routes,
            "packages": self._packages,
            "trucks": self._trucks,
        }

        file_path = "data/app_data.pickle"
        if not os.path.exists(os.path.dirname(file_path)):
            os.makedirs(os.path.dirname(file_path))

        with open("data/app_data.pickle", "wb") as file:
            pickle.dump(data, file)

    def load_data(self):
        if os.path.isfile("data/app_data.pickle"):
            with open("data/app_data.pickle", "rb") as file:
                data = pickle.load(file)
                self._routes = data["routes"]
                self._packages = data["packages"]
                self._trucks = data["trucks"]

    def reset_data(self):
        self._routes: list[Route] = []
        self._trucks: list[Trucks] = []
        self._packages: list[Package] = []
